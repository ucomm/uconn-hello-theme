/*
* This is a starter gulpfile.
* The ucomm/local docker image will run it by default on `docker-compose up`.
*/

const gulp = require('gulp');
const pluginOptions = {
  DEBUG: true,
  camelize: true,
  lazy: true
};

// Format for using `gulp-load-plugins` is plugins.name (e.g. plugins.sourcemaps or plugins.sass).
// This just saves on having to re-declare them as constants.
const plugins = require('gulp-load-plugins')(pluginOptions);

// Example gulp task
// Make sure to add any other packages (e.g. gulp-sass) if you plan to use these.
gulp.task('sass', () => {
  return gulp.src('./sass/index.scss')
    .pipe(plugins.sourcemaps.init())
      .pipe(plugins.sass())
    .pipe(plugins.sourcemaps.write('./sass/sourcemaps'))
    .pipe(gulp.dest('./', {
      overwrite: true
    }));
});

gulp.task('js', () => {
  return gulp.src('./js/**/*.js')
    .pipe(plugins.sourcemaps.init())
      .pipe(plugins.concat('index.js'))
    .pipe(plugins.sourcemaps.write('./js/sourcemaps'))
    .pipe(gulp.dest('./', {
      overwrite: true
    }));
});



gulp.task('watch', ['sass', 'js'], () => {
  gulp.watch(['./sass/**/*.scss'], ['sass']);
  gulp.watch(['./js/**/*.js'], ['js']);
});

gulp.task('build', ['sass', 'js']);

// gulp.task('default', ['watch']);

gulp.task('default', () => {
  console.log('The default gulp task ran! Please change according to your needs.\n To restart, use the command `docker-compose restart {container name}`');
});